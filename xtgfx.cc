#include <algorithm>
#include <cassert>
#include <cctype>
#include <cerrno>
#include <cstddef>
#include <cstdio>
#include <iterator>
#include <stdexcept>
#include <string>
#include <utility>
#include "xtgfx.h"
extern "C"
{
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
}

namespace
{
  using namespace xtgfx;

  void full_reset(std::FILE *const file)
  {
    std::fputs("\033c", file);
  }

  void put_style(std::FILE *const file, const style &style)
  {
    std::fputs("\033[0", file);

    if (style.bold) std::fputs(";1", file);
    if (style.faint) std::fputs(";2", file);
    if (style.underline) std::fputs(";4", file);
    if (style.blink) std::fputs(";5", file);
    if (style.inverse) std::fputs(";7", file);

    if (static_cast<int>(style.fg_color) < 9)
      std::fprintf(file, ";3%i", static_cast<int>(style.fg_color));
    if (static_cast<int>(style.fg_color) > 9)
      std::fprintf(file, ";9%i", static_cast<int>(style.fg_color) - 10);
    if (static_cast<int>(style.bg_color) < 9)
      std::fprintf(file, ";4%i", static_cast<int>(style.bg_color));
    if (static_cast<int>(style.bg_color) > 9)
      std::fprintf(file, ";10%i", static_cast<int>(style.bg_color) - 10);

    std::fputc('m', file);
  }

  void gotoxy(std::FILE *const file, const int x, const int y)
  {
    std::fprintf(file, "\033[%i;%if", y+1, x+1);
  }
};


xtgfx::window::window()
  : m_io(nullptr, std::fclose)
{
}

xtgfx::window::window(const int tty_fd)
  : window()
{
  if (::isatty(tty_fd) != 1)
    throw std::invalid_argument("xtgfx::window(): tty is not a tty");

  m_io.reset(::fdopen(tty_fd, "r+"));
  ::tcgetattr(tty_fd, &m_orig_termios);
  ::termios raw_termios;
  ::cfmakeraw(&raw_termios);
  ::tcsetattr(tty_fd, TCSAFLUSH, &raw_termios);

  reinit();
}

xtgfx::window::~window()
{
  if (m_io)
  {
    full_reset(m_io.get());
    std::fflush(m_io.get());
    ::tcsetattr(::fileno(m_io.get()), TCSAFLUSH, &m_orig_termios);
  }
}

xtgfx::window &xtgfx::window::operator=(xtgfx::window &&other) noexcept
{
  if (m_io)
  {
    full_reset(m_io.get());
    std::fflush(m_io.get());
    ::tcsetattr(::fileno(m_io.get()), TCSAFLUSH, &m_orig_termios);
  }

  m_io = std::move(other.m_io);
  m_orig_termios = other.m_orig_termios;
  m_input_q = std::move(other.m_input_q);
  m_showing_cursor = other.m_showing_cursor;
  m_cursor_x = other.m_cursor_x;
  m_cursor_y = other.m_cursor_y;

  return *this;
}

void xtgfx::window::maybe_hide_cursor()
{
  if (m_showing_cursor)
    std::fputs("\033[?25l", m_io.get());
}

void xtgfx::window::maybe_show_cursor()
{
  if (m_showing_cursor)
  {
    gotoxy(m_io.get(), m_cursor_x, m_cursor_y);
    std::fputs("\033[?25h", m_io.get());
  }
}

xtgfx::window xtgfx::window::make_from_tty_fd(const int tty_fd)
{
  return window(tty_fd);
}

xtgfx::window xtgfx::window::make_from_stdio()
{
  std::fflush(stdout);
  std::fflush(stderr);
  return window(::fileno(stdin));
}

void xtgfx::window::reset()
{
  *this = window();
}

void xtgfx::window::reinit()
{
  std::fputs(
    "\033c"  // full reset
    "\033[?47h"  // alternate screen buffer
    "\033[?25l"  // hide cursor
    , m_io.get());
  flush(true);
  discard_input();
  m_showing_cursor = false;
}

void xtgfx::window::flush(const bool sync)
{
  std::fflush(m_io.get());
  if (sync) ::tcdrain(::fileno(m_io.get()));
}

void xtgfx::window::clear()
{
  std::fputs("\033[m\033[2J", m_io.get());
}

void xtgfx::window::clear_below(const int y)
{
  if (y < 1) { clear(); return; }
  maybe_hide_cursor();
  // apparently this behaves differently in alternate mode???
  std::fprintf(m_io.get(), "\033[m\033[%id\033[J", y);
  maybe_show_cursor();
}

void xtgfx::window::clear_above(const int y)
{
  if (y < 1) return;
  maybe_hide_cursor();
  std::fprintf(m_io.get(), "\033[m\033[%id\033[1J\033[K", y);
  maybe_show_cursor();
}

void xtgfx::window::clear_line(const int y)
{
  if (y < 0) return;
  maybe_hide_cursor();
  std::fprintf(m_io.get(), "\033[m\033[%id\033[2K", y+1);
  maybe_show_cursor();
}

void xtgfx::window::clear_line_right_of(const int x, const int y)
{
  if (y < 0) return;
  if (x < 1) { clear_line(y); return; }
  maybe_hide_cursor();
  gotoxy(m_io.get(), x, y);
  std::fputs("\033[m\033[K", m_io.get());
  maybe_show_cursor();
}

void xtgfx::window::clear_line_left_of(const int x, const int y)
{
  if (y < 0 || x < 1) return;
  maybe_hide_cursor();
  gotoxy(m_io.get(), x-1, y);
  std::fputs("\033[m\033[1K", m_io.get());
  maybe_show_cursor();
}

void xtgfx::window::put_text(const int x, int y, const std::string &text, const style &style)
{
  // FIXME: handle out-of-range X & Y... chop off excess

  put_style(m_io.get(), style);

  maybe_hide_cursor();

  using std::begin;
  using std::end;
  auto line_start = begin(text);
  for (;;)
  {
    const auto line_end = std::find(line_start, end(text), '\n');
    gotoxy(m_io.get(), x, y);
    std::fwrite(&*line_start, &*line_end - &*line_start, 1, m_io.get());
    if (line_end == end(text)) break;
    line_start = line_end + 1;
    ++y;
  }

  maybe_show_cursor();
}

int xtgfx::window::poll_fd() const noexcept
{
  return ::fileno(m_io.get());
}

bool xtgfx::window::need_poll() const noexcept
{
  return m_input_q.empty();
}

void xtgfx::window::discard_input()
{
  ::tcflush(::fileno(m_io.get()), TCIFLUSH);
  m_input_q.clear();
}

event xtgfx::window::read_event()
{
  using std::begin;
  using std::end;

  // sorry for this mess...

  event ret;
  bool would_block = false;
  for (;;)
  {
    if (!m_input_q.empty())
    {
      // process what we have in the queue
      if (!std::iscntrl(m_input_q.front()))
      {
        ret.type = event::type::text_input;
        const auto last = std::find_if(begin(m_input_q), end(m_input_q),
            [](const unsigned char x) { return std::iscntrl(x); });
        ret.text.assign(begin(m_input_q), last);
        m_input_q.erase(begin(m_input_q), last);
        return ret;
      }
      else
      {
        switch (m_input_q.front())
        {
        case 0x09:
          ret.type = event::type::special_keypress;
          ret.special_key = special_key::tab;
          m_input_q.pop_front();
          return ret;

        case 0x0D:
          ret.type = event::type::special_keypress;
          ret.special_key = special_key::enter;
          m_input_q.pop_front();
          return ret;

        case 0x7F:
          ret.type = event::type::special_keypress;
          ret.special_key = special_key::backspace;
          m_input_q.pop_front();
          return ret;

        case 0x1B:
          if (m_input_q.size() < 2) break;

          if (m_input_q[1] == 0x5B || m_input_q[1] == 0x4F)
          {
            const auto param_begin = begin(m_input_q) + 2;
            const auto param_end =
              std::find_if(param_begin, end(m_input_q),
                [](const int c) { return !std::isdigit(c) && c != ';'; });

            if (param_end == end(m_input_q) || (!std::isalpha(*param_end) && *param_end != '~'))
            {
              // 10 is enough for two 3-digit parameters...
              // if queue is larger than this, treat as meta-[
              if (m_input_q.size() > 10)
              {
                ret.type = event::type::modified_keypress;
                ret.modified_key = '[';
                ret.modifiers.alt = true;
                m_input_q.pop_front();
                m_input_q.pop_front();
                return ret;
              }
              else break;  // otherwise read more; perhaps buffer was full before
            }

            int code, mod;

            const auto param1_end = std::find(param_begin, param_end, ';');
            if (param_begin == param1_end) code = 1;
            else
            {
              try { code = std::stoi(std::string(param_begin, param1_end)); }
              catch (std::invalid_argument &e) { code = 0; }
            }

            if (param1_end == param_end)
            {
              // one parameter, code
              mod = 1;
            }
            else
            {
              // two parameters, code and mod
              const auto param2_begin = param1_end + 1;
              if (param2_begin == param_end) mod = 1;
              else
              {
                try { mod = std::stoi(std::string(param2_begin, param_end)); }
                catch (std::invalid_argument &e) { mod = 0; }
              }
            }

            if (code < 1 || mod < 1)
            {
              ret.type = event::type::modified_keypress;
              ret.modified_key = '[';
              ret.modifiers.alt = true;
              m_input_q.pop_front();
              m_input_q.pop_front();
              return ret;
            }

            const char cmd = *param_end;

            // at this point we will either consume or discard entire command,
            // so remove from queue (rather than reinterpreting as meta-[)
            m_input_q.erase(begin(m_input_q), param_end + 1);

            // by default, KP flag is based on CSI vs SS3
            // but sub-commands can override it, so set first
            ret.modifiers.kp = m_input_q[1] == 0x4F;

            switch (cmd)
            {
            case 'A': ret.special_key = special_key::up_arrow; break;
            case 'B': ret.special_key = special_key::down_arrow; break;
            case 'C': ret.special_key = special_key::right_arrow; break;
            case 'D': ret.special_key = special_key::left_arrow; break;
            case 'E': ret.special_key = special_key::begin; ret.modifiers.kp = true; break;
            case 'F': ret.special_key = special_key::end; break;
            case 'H': ret.special_key = special_key::home; break;
            case 'I': ret.special_key = special_key::tab; break;
            case 'M': ret.special_key = special_key::enter; break;
            case 'P': ret.special_key = special_key::f1; ret.modifiers.kp = false; break;
            case 'Q': ret.special_key = special_key::f2; ret.modifiers.kp = false; break;
            case 'R': ret.special_key = special_key::f3; ret.modifiers.kp = false; break;
            case 'S': ret.special_key = special_key::f4; ret.modifiers.kp = false; break;
            case 'X': ret.special_key = special_key::equal; break;
            case 'j': ret.special_key = special_key::multiply; break;
            case 'k': ret.special_key = special_key::plus; break;
            case 'l': ret.special_key = special_key::comma; break;
            case 'm': ret.special_key = special_key::minus; break;
            case 'o': ret.special_key = special_key::divide; break;

            case '~':
              switch (code)
              {
              case 1: ret.special_key = special_key::home; break;
              case 2: ret.special_key = special_key::insert; break;
              case 3: ret.special_key = special_key::delete_; break;
              case 4: ret.special_key = special_key::end; break;
              case 5: ret.special_key = special_key::page_up; break;
              case 6: ret.special_key = special_key::page_down; break;
              case 15: ret.special_key = special_key::f5; break;
              case 17: ret.special_key = special_key::f6; break;
              case 18: ret.special_key = special_key::f7; break;
              case 19: ret.special_key = special_key::f8; break;
              case 20: ret.special_key = special_key::f9; break;
              case 21: ret.special_key = special_key::f10; break;
              case 23: ret.special_key = special_key::f11; break;
              case 24: ret.special_key = special_key::f12; break;
              case 25: ret.special_key = special_key::f13; break;
              case 26: ret.special_key = special_key::f14; break;
              case 28: ret.special_key = special_key::f15; break;
              case 29: ret.special_key = special_key::f16; break;
              case 31: ret.special_key = special_key::f17; break;
              case 32: ret.special_key = special_key::f18; break;
              case 33: ret.special_key = special_key::f19; break;
              case 34: ret.special_key = special_key::f20; break;
              default: ret.modifiers.kp = false; continue;  // garbage; toss it
              }
              break;

            default: ret.modifiers.kp = false; continue;  // garbage; toss it
            }

            ret.type = event::type::special_keypress;
            ret.modifiers.shift = (mod - 1) & 1;
            ret.modifiers.alt = (mod - 1) & 2;
            ret.modifiers.control = (mod - 1) & 4;
            // FIXME: meta??

            return ret;
          }
          else
          {
            // must be a "meta" sequence...

            m_input_q.pop_front();

            if (m_input_q.front() >= 0x80)
            {
              // escape followed by "meta"? garbage, toss it...
              m_input_q.pop_front();
              continue;
            }

            const char c = m_input_q.front();
            m_input_q.pop_front();

            if (std::islower(c))
            {
              ret.type = event::type::modified_keypress;
              // for symmetry with control
              ret.modified_key = c - 0x20;
            }
            else if (std::isupper(c))
            {
              ret.type = event::type::modified_keypress;
              ret.modified_key = c;
              ret.modifiers.shift = true;
            }
            else
            {
              switch (c)
              {
              case 0x09:
                ret.type = event::type::special_keypress;
                ret.special_key = special_key::tab;
                break;

              case 0x0D:
                ret.type = event::type::special_keypress;
                ret.special_key = special_key::enter;
                break;

              case 0x7F:
                ret.type = event::type::special_keypress;
                ret.special_key = special_key::backspace;
                break;

              case 0x1B:
                ret.type = event::type::special_keypress;
                ret.special_key = special_key::escape;
                break;

              default:
                ret.type = event::type::modified_keypress;
                if (c < 0x20)
                {
                  ret.modified_key = c + 0x40;
                  ret.modifiers.control = true;
                }
                else
                {
                  // just return these as is...
                  // don't try to guess shifted/unshifted keycap pairs...
                  ret.modified_key = c;
                }
                break;
              }
            }

            ret.modifiers.alt = true;
            return ret;
          }

        default:
          ret.type = event::type::modified_keypress;
          ret.modified_key = static_cast<char>(m_input_q.front() + 0x40);
          ret.modifiers.control = true;
          m_input_q.pop_front();
          return ret;
        }
      }
    }

    // assume if we already got a short read, that our next read will block
    if (would_block) break;

    const int fd = ::fileno(m_io.get());

    // the only time we get here with an empty queue
    // is if the user called us that way... so block!
    // (also, since we never try to complete escape sequences across
    // a short read, we don't need to block if our queue is non-empty)
    if (!m_input_q.empty())
      ::fcntl(fd, F_SETFL, O_NONBLOCK);

    unsigned char buf[1024];
    const int read_ret = ::read(fd, buf, sizeof buf);

    if (read_ret < 0 && (errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR))
    {
      if (!m_input_q.empty())
        ::fcntl(fd, F_SETFL, 0);
      // not actually a need to poll after EINTR, but keeps user code simple
      // (we DO need to return, in case user is using signals)
      break;
    }

    if (read_ret <= 0)
    {
      if (!m_input_q.empty())
        ::fcntl(fd, F_SETFL, 0);
  
      // TODO: better error handling
      if (m_input_q.empty())
      {
        ret.type = event::type::window_close;
        return ret;
      }
      else break;
    }

    if (!m_input_q.empty())
      ::fcntl(fd, F_SETFL, 0);

    std::copy(&buf[0], &buf[read_ret], std::back_inserter(m_input_q));

    if (static_cast<std::size_t>(read_ret) < sizeof buf)
      would_block = true;
  }

  // we only get here if we cannot process what is left in the queue
  // so either, queue is empty, or it contains an incomplete escape sequence
  // the latter we treat as a non-escape sequence...
  // this is fine for modern "terminals" which enqueue escape sequences atomically
  // but not for traditional "slow" terminals... oh well!

  if (m_input_q.empty())
  {
    ret.type = event::type::poll;
    return ret;
  }

  assert(m_input_q.front() == 0x1B);
  m_input_q.pop_front();

  if (m_input_q.empty())
  {
    ret.type = event::type::special_keypress;
    ret.special_key = special_key::escape;
    return ret;
  }
  else
  {
    // must be a "meta" sequence
    // we should only get here for alphanumeric cases;
    // all control and meta cases would have been handled immediately above
    // since they can't form control sequences
    assert(m_input_q.front() < 0x80);
    assert(!std::iscntrl(m_input_q.front()));

    const char c = m_input_q.front();
    m_input_q.pop_front();

    if (std::islower(c))
    {
      // for symmetry with control
      ret.modified_key = c - 0x20;
    }
    else if (std::isupper(c))
    {
      ret.modified_key = c;
      ret.modifiers.shift = true;
    }
    else
    {
      // just return these as is...
      // don't try to guess shifted/unshifted keycap pairs...
      ret.modified_key = c;
    }

    ret.type = event::type::modified_keypress;
    ret.modifiers.alt = true;
    return ret;
  }
}

void xtgfx::window::show_cursor_at(const int x, const int y) noexcept
{
  gotoxy(m_io.get(), x, y);
  if (!m_showing_cursor)
    std::fputs("\033[?25h", m_io.get());

  m_showing_cursor = true;
  m_cursor_x = x;
  m_cursor_y = y;
}

void xtgfx::window::hide_cursor() noexcept
{
  maybe_hide_cursor();
  m_showing_cursor = false;
}
