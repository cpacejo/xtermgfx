#ifndef XTGFX_H
#define XTGFX_H

#include <cstdio>
#include <deque>
#include <memory>
#include <string>
extern "C"
{
#include <termios.h>
#include <unistd.h>
}

namespace xtgfx
{
  struct style
  {
    enum class color
    {
      black = 0, maroon = 1, forest_green = 2, mustard = 3,
      navy_blue = 4, purple = 5, aqua = 6, light_grey = 7,
      default_ = 9,
      dark_grey = 10, red = 11, green = 12, yellow = 13,
      blue = 14, magenta = 15, cyan = 16, white = 17
    };

    color fg_color = color::default_, bg_color = color::default_;

    bool bold = false, faint = false, underline = false, blink = false, inverse = false;
  };

  enum class special_key
  {
    escape, backspace, tab, enter,
    left_arrow, right_arrow, up_arrow, down_arrow,
    insert, delete_, home, end, page_up, page_down,
    f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12,
    f13, f14, f15, f16, f17, f18, f19, f20,
    // the following are only emitted with modifier kp
    space, plus, minus, multiply, divide, equal, comma, begin
  };

  struct modifiers
  {
    bool shift: 1;
    bool alt: 1;
    bool control: 1;
    bool kp: 1;  // is this actually different from alt?

    modifiers() noexcept:
      shift{false}, alt{false}, control{false}, kp{false}
    { }
  };

  struct event
  {
    enum class type
    {
      poll,  // indicates that application must poll before calling again!
      text_input,
      special_keypress,
      modified_keypress,
      resize,
      redraw_needed,
      window_close
    };

    enum type type;
    std::string text;  // text_input
    struct modifiers modifiers;  // special_keypress, modified_keypress
    enum special_key special_key;  // special_keypress
    char modified_key;  // modified_keypress; always uppercase letters
  };

  class window
  {
    std::unique_ptr<std::FILE, int (*)(std::FILE *)> m_io;
    ::termios m_orig_termios;
    std::deque<unsigned char> m_input_q;
    bool m_showing_cursor;
    int m_cursor_x, m_cursor_y;

    explicit window(int tty_fd);

    void maybe_hide_cursor();
    void maybe_show_cursor();

  public:
    window();
    ~window();

    window(const window &) = delete;
    window(window &&) = default;

    window &operator=(const window &) = delete;
    window &operator=(window &&) noexcept;

    // Note: you MUST flush all writeable streams associated with the tty
    // before calling this function (typically cout and cerr).  Else weird
    // race conditions make screen initialization sometimes break.
    static window make_from_tty_fd(int tty_fd = STDIN_FILENO);
    // This function does that for you.
    static window make_from_stdio();

    // Reset to empty object (releasing TTY and clearing screen).
    void reset();

    // Reinitialize TTY, clearing screen.
    void reinit();

    // Flush pending operations to screen.
    // Optionally wait for flush to complete, e.g. if you are
    // going to discard input before a prompt.
    void flush(bool sync = false);

    // Clear screen.
    void clear();
    void clear_below(int y);  // inclusive
    void clear_above(int y);  // exclusive
    void clear_line(int y);
    void clear_line_right_of(int x, int y);  // inclusive
    void clear_line_left_of(int x, int y);  // exclusive

    // Put text on screen.  Embedded newlines wrap
    // to next y location, original x location.
    void put_text(int x, int y, const std::string &, const style & = style());

    // File descriptor for polling.
    // If POLLIN is triggered, read_event() will not block.
    int poll_fd() const noexcept;

    // read_event() may block if this returns true.
    bool need_poll() const noexcept;

    // Discard any unread events.
    // Useful immediately after a prompt has been flushed + synced to the screen.
    void discard_input();

    // Return next event from queue.
    // May block, unless need_poll() and polling poll_fd() indicates otherwise.
    event read_event();

    void show_cursor_at(int x, int y) noexcept;
    void hide_cursor() noexcept;
  };
};

#endif
