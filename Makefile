CPPFLAGS = -std=c++11 -Wall -Wextra -Werror -Os

OBJS = xtgfx.o

xtgfx.a: $(OBJS)
	ar rcs $@ $(OBJS)

xtgfx.o: xtgfx.h
